
# OpenWeatherApp

DEMO: https://open-weather-board.herokuapp.com/

The application is made by using chart js library and ow-client-lib npm package (the creation of which is inspired by the job interview task). It consumes openweathermap.org multiple api and is focused on scalability for further enhancements.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.0.

