import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatToolbarModule } from '@angular/material/toolbar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';
import { MatDividerModule } from '@angular/material/divider';
import { FormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';

import { OwClientLibModule } from 'ow-client-lib';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { OpenWeatherComponent } from './components/open-weather.component';
import { TempChartComponent } from './components/charts/tempchart/temp-chart.component';
import { TodayComponent } from './components/current-day/current-day.component';
import { WindChartComponent } from './components/charts/windchart/wind-chart.component';
import { HumidityChartComponent } from './components/charts/humiditychart/humidity-chart.component';
import { PressureChartComponent } from './components/charts/pressurechart/pressure-chart.component';

import { appRoutes } from './app.routing';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    OpenWeatherComponent,
    TempChartComponent,
    TodayComponent,
    WindChartComponent,
    HumidityChartComponent,
    PressureChartComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    OwClientLibModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes),
    MatToolbarModule,
    MatInputModule,
    MatCardModule,
    MatTabsModule,
    MatDividerModule,
    ToastrModule.forRoot({
      timeOut: 3000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
    })
  ],
  providers: [
    { provide: 'APP_ID', useValue: '9e1e3746adb21cc5a5c1f00446c1caef' }, // this is api key of openweathermap.org
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
