export interface TodaysDetails {
    pressure: number;
    humidity: number;
    wind_speed: number;
    clouds: number;
    icon: string;
}
