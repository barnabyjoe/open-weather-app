import {TodaysDetails} from './today-details.interface';

export class TodaysContainer {
    details: TodaysDetails;
    location: {city: string; country: string};
    temprature: number;
    state: string;
}
