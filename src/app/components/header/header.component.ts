import {Component, OnInit} from '@angular/core';
import {tap, catchError} from 'rxjs/operators';
import {OwClientLibService, UnitType} from 'ow-client-lib';
import {ToastrService} from 'ngx-toastr';
import {of} from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private _owClientLibService: OwClientLibService, private toastr: ToastrService) {}

  public ngOnInit(): void {
    const defaultInputs = ['London', 'UK'];
    this.getWeatherByName(defaultInputs);
  }

  public handleInputChange(place: string): void {
    const partsOfStr = place.split(/[,]+/);
    const regexp = /\d+/;
    if (!partsOfStr[0].match(regexp)) {
      this.getWeatherByName(partsOfStr);
    } else {
      this.getWeatherByZipCode(partsOfStr);
    }
  }

  private getWeatherByName(inputs: Array<string>): void {
    this._owClientLibService.GetFiveDaysForecastByCityName(inputs[0], inputs[1], UnitType.metric).pipe(
      tap(x => this._owClientLibService.GetTodayForecastByCityName(inputs[0], inputs[1], UnitType.metric).subscribe(
        () => {},
        (err) => {
          this.toastr.error(err.error.message);
        }
      )
      ),
      catchError((err) => {
        this.toastr.error(err.error.message);
        return of();
      })).subscribe(
        () => {},
        (err) => {
          this.toastr.error(err.message);
        }
      );
  }

  private getWeatherByZipCode(inputs: Array<string>): void {
    this._owClientLibService.GetFiveDaysForecastByZipCode(inputs[0], inputs[1], UnitType.metric).pipe(
      tap(x => this._owClientLibService.GetTodayForecastByZipCode(inputs[0], inputs[1], UnitType.metric).subscribe(
        () => {},
        (err) => {
          this.toastr.error(err.error.message);
        }
      )
      ),
      catchError((err) => {
        this.toastr.error(err.error.message);
        return of();
      })).subscribe(
        () => {},
        (err) => {
          this.toastr.error(err.message);
        }
      );
  }
}
