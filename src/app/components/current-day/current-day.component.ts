import {Component, OnInit} from '@angular/core';
import {OwClientLibService} from 'ow-client-lib';
import {TodaysContainer} from 'src/app/models/todays-container.class';
import {Icon} from '../../models/icon.enum';

@Component({
    selector: 'app-current-day',
    templateUrl: './current-day.component.html',
    styleUrls: ['./current-day.component.scss']
})
export class TodayComponent implements OnInit {
    today: number = Date.now();
    todaysContainer: TodaysContainer;

    constructor(private _owClientLibService: OwClientLibService) {}

    public ngOnInit(): void {
        this._owClientLibService.currentStream.subscribe(resp => {
            this.todaysContainer = new TodaysContainer();
            this.todaysContainer.temprature = resp.main.temp;
            this.todaysContainer.details = {
                clouds: resp.clouds.all,
                humidity: resp.main.humidity,
                pressure: resp.main.pressure,
                wind_speed: resp.wind.speed,
                icon: resp.weather[0].icon
            };
            this.todaysContainer.state = resp.weather[0].main;
            this.todaysContainer.location = {city: resp.name, country: resp.sys.country};
        });
    }

    public pickIcon(iconCode: Icon): string {
        switch (iconCode) {
            case Icon.Clear_sky_day: return 'wi wi-day-sunny';
            case Icon.Clear_sky_night: return 'wi wi-night-clear';
            case Icon.Few_clouds_day: return 'wi wi-day-cloudy';
            case Icon.Few_clouds_night: return 'wi wi-night-cloudy';
            case Icon.Scattered_clouds_day: return 'wi wi-cloudy';
            case Icon.Scattered_clouds_night: return 'wi wi-cloudy';
            case Icon.Broken_clouds_day: return 'wi wi-showers';
            case Icon.Broken_clouds_night: return 'wi wi-showers';
            case Icon.rain_day: return 'wi wi-day-rain';
            case Icon.rain_night: return 'wi wi-night-rain';
            case Icon.Thunderstorm_day: return 'wi wi-day-snow-thunderstorm';
            case Icon.Thunderstorm_night: return 'wi wi-night-snow-thunderstorm';
            case Icon.Snow_day: return 'wi wi-day-snow';
            case Icon.Snow_night: return 'wi wi-night-snow';
            case Icon.Mist_day: return 'wi wi-day-fog';
            case Icon.Mist_night: return 'wi wi-night-fog';
        }
    }
}


