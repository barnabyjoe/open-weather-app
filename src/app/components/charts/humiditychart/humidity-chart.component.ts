import { Component, OnInit, ElementRef } from '@angular/core';
import { OwClientLibService } from 'ow-client-lib';
import { Chart } from 'chart.js';

@Component({
    selector: 'app-humidity-chart',
    templateUrl: './humidity-chart.component.html'
})
export class HumidityChartComponent implements OnInit {

    constructor(private _owClientLibService: OwClientLibService, private elementRef: ElementRef) { }

    chart: Chart;

    public ngOnInit(): void {

        this._owClientLibService.fiveDayStream.subscribe(response => {
            const temps = response.list.map(x => x.main.humidity);
            const dates = response.list.map(x => new Date(x.dt * 1000));
            const ctx = this.elementRef.nativeElement.querySelector(`#humidity`);
            if (this.chart) {
                this.chart.destroy();
            }
            this.chart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: dates,
                    datasets: [{
                        data: temps,
                        borderColor: '#CCC',
                        fill: false
                    }]
                },
                options: {
                    legend: {
                        display: false
                    },
                    scales: {
                        xAxes: [{
                            display: true,
                            type: 'time',
                            time: {
                                unit: 'day',
                                displayFormats: {
                                    day: 'MMM D'
                                }
                            }
                        }],
                        yAxes: [{
                            display: true
                        }]
                    }
                }
            });
        }
        );
    }
}
