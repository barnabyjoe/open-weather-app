import { Routes } from '@angular/router';
import { OpenWeatherComponent } from './components/open-weather.component';

export const appRoutes: Routes = [
    { path: '', component: OpenWeatherComponent },
    { path: '**', redirectTo: '' }
];
